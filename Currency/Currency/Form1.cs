﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Currency.Curency;
using System.Globalization;

namespace Currency
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {

            WebRequest request = WebRequest.Create("http://data.fixer.io/api/latest?access_key=521a286f6c9a2779e426e3da5a8a9933");

            request.Method = "Post";
            request.ContentType = "application/x-www-urlencoding";

            WebResponse response = await request.GetResponseAsync();

            string answer = string.Empty;

            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    answer = await reader.ReadToEndAsync();
                }
            }

            response.Dispose();
            richTextBox1.Text = answer;

            Ratess rates = JsonConvert.DeserializeObject<Ratess>(answer);

            label1.Text = rates.Success.ToString();
            label2.Text = rates.Base.ToString();
            label3.Text = "AMD " + rates.Rates.AMD.ToString();
            label4.Text = "USD " + rates.Rates.USD.ToString();
            label5.Text = "GEL " + rates.Rates.GEL.ToString();
            label6.Text = "GBP " + rates.Rates.GBP.ToString();
            label7.Text = DateTime.Now.ToString();

            //comboBox1.Items.Add(rates.Rates.AMD.GetType().g);
            var pis = rates.Rates.GetType().GetProperties();
            string isoCurrencySymbol = RegionInfo.CurrentRegion.CurrencySymbol;
            foreach (var item in pis)
            {
                comboBox1.Items.Add((item.Name) + " --- " + item.GetValue(rates.Rates));
            }


        }
    }
}
