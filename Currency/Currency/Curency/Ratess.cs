﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Currency.Curency
{
    class Ratess
    {
        [JsonProperty("success")]
        public bool Success;
        [JsonProperty("timestamp")]
        public decimal Timestamp;
        [JsonProperty("base")]
        public string Base;
        public Rates Rates { get; set; }
    }
}
